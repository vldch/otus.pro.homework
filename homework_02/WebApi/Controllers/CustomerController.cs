using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.DAL.Repositories.DbRepositories;
using WebApi.DAL.Repositories.Interfaces;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [HttpGet("{id:long}")]
        public async Task<IActionResult> GetCustomerAsync([FromRoute] long id)
        {
            var customer = await _customerRepository.GetAsync(id);
            if (customer == null) return NotFound();
            return Ok(customer);
        }

        [HttpPost("")]   
        public async Task<IActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            try
            {
                long id = await _customerRepository.CreateAsync(customer);
                
                return Ok(id); 
            }
            catch (CustomerIsExistException ex)
            {
                return Conflict(new { message = ex.Message });
            }
        }
    }
}