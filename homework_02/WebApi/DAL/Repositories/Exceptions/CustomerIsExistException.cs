﻿using System;

namespace WebApi.DAL.Repositories.DbRepositories
{
    public class CustomerIsExistException : Exception
    {
        public CustomerIsExistException()
        {
        }

        public CustomerIsExistException(string message) : base(message)
        {
        }
    }
}
