﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DAL.Repositories.Interfaces;
using WebApi.Models;

namespace WebApi.DAL.Repositories.DbRepositories
{
    public sealed class CustomerRepository : ICustomerRepository
    {
        private static string _pathDb;

        private static CustomerRepository _instance;

        private static List<Customer> _customers;

        private CustomerRepository(string pathDb)
        {

        }

        public static CustomerRepository GetInstance(string pathDb)
        {

            if (_instance == null && pathDb != null)
            {
                _pathDb = pathDb;

                _instance = new CustomerRepository(pathDb);

                InitializeDB();

                Task.Run(() => WriteToFile(_customers)).Wait();
            }

            return _instance;
        }

        private static void InitializeDB()
        {
            _customers = new()
            {
                new Customer { Id = 1, Firstname = "First_Name_1", Lastname = "Last_Name_1" },
                new Customer { Id = 2, Firstname = "First_Name_2", Lastname = "Last_Name_2" },
                new Customer { Id = 3, Firstname = "First_Name_3", Lastname = "Last_Name_3" },
                new Customer { Id = 4, Firstname = "First_Name_4", Lastname = "Last_Name_4" },
                new Customer { Id = 5, Firstname = "First_Name_5", Lastname = "Last_Name_5" },
            };

            Task.Run(() => WriteToFile(_customers)).Wait();
        }

        public async Task<long> CreateAsync(Customer entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("Parameter cannot be null", nameof(entity));
            }

            _customers = await Task.Run(() => ReadFromFile<List<Customer>>());

            var customer = await GetAsync(entity.Id);

            if (customer != null)
            {
                throw new CustomerIsExistException("Customer with Id=" + entity.Id + " is already exist.");
            }

            _customers.Add(entity);

            await Task.Run(() => WriteToFile(_customers));

            return entity.Id;
        }

        public async Task DeleteAsync(long id)
        {
            _customers = await Task.Run(() => ReadFromFile<List<Customer>>());
            var customer = _customers.Find(x => x.Id == id);
            if (customer == null) throw new ArgumentException("CustomerId == " + id + " cannot be found.");
            _customers.Remove(customer);
            await Task.Run(() => WriteToFile(_customers));
        }

        public async Task<Customer> GetAsync(long id)
        {
            await Task.Delay(1000);
            _customers = ReadFromFile<List<Customer>>();
            return _customers.Find(x => x.Id == id);
        }

        private static void WriteToFile<T>(T obj, bool append = false) where T : new()
        {
            TextWriter writer = null;
            try
            {
                var serilizedObject = JsonConvert.SerializeObject(obj);
                writer = new StreamWriter(_pathDb, append);
                writer.Write(serilizedObject);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        private static T ReadFromFile<T>() where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(_pathDb);
                var readedFile = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(readedFile);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

    }
}
