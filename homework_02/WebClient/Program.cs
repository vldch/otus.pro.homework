﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        /// <summary>
        /// BaseUrl is base address of url
        /// </summary>
        const string baseUrl = "https://localhost:5001";

        private const string requestUri = "customers";

        private static readonly HttpClient _httpClient = new HttpClient();

        static async Task Main(string[] args)
        {
            _httpClient.BaseAddress = new Uri(baseUrl);

            while (true)
            {
                var customer = SetCustomer();

                long customerId = PostCustomerRequest(customer).Result;

                Console.WriteLine("CustomerIdResponse = " + customerId);

                //To request added customer
                Console.WriteLine("Request added customer Id with the number = " + customerId);
                
                customer = GetCustomerRequest(customerId).Result;
                
                Console.WriteLine("Created a customer:");

                Console.WriteLine("id: '" + customer.Id + "'\nFirstname: '" +
                    customer.Firstname + "'\nLastname: '" + customer.Lastname + "'");

                Console.WriteLine("If you want to exit? Please type Exit:");

                var exit = Console.ReadLine();

                if (exit.ToLower() == "exit")
                    break;
            }
        }

        private static Customer SetCustomer()
        {
            Customer customer;

            Console.WriteLine("Do you want to create a random user? Please enter: yes or no");
            
            var randomUser = Console.ReadLine();

            if (randomUser.ToLower() == "yes")
            {
                customer = new Customer()
                {
                    Firstname = GetRandomString(7),
                    Lastname = GetRandomString(10)
                };
            }
            else
            {
                Console.WriteLine("Please enter a first name:");
                
                var firstName = Console.ReadLine();
                
                Console.WriteLine("Please enter a last name:");
                
                var lastName = Console.ReadLine();
                
                customer = new Customer()
                {
                    Firstname = firstName,
                    Lastname = lastName
                };
            }

            return customer;
        }

        private static string GetRandomString(int lenght)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            
            var stringChars = new char[lenght];
            
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new String(stringChars);
        }

        private static async Task<Customer> GetCustomerRequest(long customerId)
        {
            Customer customer;

            HttpResponseMessage response = await _httpClient.GetAsync(requestUri + "/" + customerId);

            Console.WriteLine("Response StatusCode: " + response.StatusCode);

            if (response.IsSuccessStatusCode)
            {
                String jsonString = await response.Content.ReadAsStringAsync();
                
                customer = JsonConvert.DeserializeObject<Customer>(jsonString);
            }
            else
            {
                throw new Exception("URL: " + _httpClient.BaseAddress + requestUri + "/" + customerId + "Response status " + response.StatusCode);
            }

            return customer;
        }

        private static async Task<long> PostCustomerRequest(Customer customer)
        {
            long customerRespondId;
            
            using (var content = new StringContent(
                JsonConvert.SerializeObject(customer),
                System.Text.Encoding.UTF8,
                "application/json"))
            {
                HttpResponseMessage responce = await _httpClient.PostAsync(requestUri, content);
                Console.WriteLine("Response StatusCode: " + responce.StatusCode);

                if (responce.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = responce.Content.ReadAsStringAsync().Result;
                    customerRespondId = (long)Convert.ToDouble(result);
                }
                else
                {
                    throw new Exception($"Failed to POST data: ({responce.StatusCode})");
                }
            }

            return customerRespondId;
        }
    }
}