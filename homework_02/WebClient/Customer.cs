using System;

namespace WebClient
{
    public class Customer
    {
        public long Id { get; init; } = new Random().Next();

        public string Firstname { get; init; }

        public string Lastname { get; init; }
    }
}