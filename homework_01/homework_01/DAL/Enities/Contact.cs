﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework_01.DAL.Model
{
    public class Contact : EntityBase
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string StreetName { get; set; }
        public string PostCode { get; set; }
        public virtual City City { get; set; }
        public virtual Country Country { get; set; }
    }
}
