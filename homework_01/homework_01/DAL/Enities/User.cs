﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework_01.DAL.Model
{
    public class User : EntityBase
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
