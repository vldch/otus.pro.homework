﻿using homework_01.DAL.Model;
using System;

namespace homework_01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var user = new User()
            {
                Name = "Kirill",
                LastName = "Skorobogatko",
                Contact = new Contact()
                {
                    PhoneNumber = "79688645456",
                    PostCode = "16518",
                    City = new City() { Name = "Volgograd" },
                    Country = new Country { Name = "Russia" },
                    Email = "skorobogatkoKir@yandex.ru",
                    StreetName = "Bolshaya Yakimanskaya 10"
                }
            };
            Console.WriteLine("Name:{0}", user.LastName);
        }
    }
}
