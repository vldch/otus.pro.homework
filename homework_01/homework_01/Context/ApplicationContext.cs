﻿using homework_01.DAL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework_01.Context
{
    public class ApplicationContext:DbContext
    {
        public ApplicationContext() : base()
        {
            //DBInitialize.Initialize(this);
        }

        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseNpgsql("Server = 127.0.0.1; Port = 5432; Database = otushomework; User Id = postgres; Password = 12345678")
                .EnableSensitiveDataLogging();
        }
    }
}
